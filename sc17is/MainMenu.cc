#include "MainMenu.h"
#include "globals.h"
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "string.h"
#include "qtgit.h"
using namespace t;


QtGit::QtGit():QtObject(){
}

MainMenu::MainMenu():QWidget(){
	QVBoxLayout* h=new QVBoxLayout;

	QPushButton* button1 = new QPushButton("Create / Select Repo");
	QPushButton* button2 = new QPushButton("Display Commits");
	QPushButton* button3 = new QPushButton("Create / Select Branch");
	QPushButton* button4 = new QPushButton("Search Keyword");
	QPushButton* button5 = new QPushButton("Show Edit Settings");
	QObject::connect(button2, SIGNAL(clicked()),this, SLOT(clickedSlot2()));


// Button1* button1 = new Button1();
// h->addWidget(test);
h->addWidget(button1);
h->addWidget(button2);
h->addWidget(button3);
h->addWidget(button4);
h->addWidget(button5);
setLayout(h);
}

void MainMenu::clickedSlot2()
{
	QMessageBox msgBox;
	msgBox.setWindowTitle("MessageBox Title");
	// msgBox.setText("You Clicked "+ ((QPushButton*)sender())->text());
	msgBox.setText("TESTING");
	msgBox.exec();
}
INSTALL_TAB(MainMenu, __FILE__);
