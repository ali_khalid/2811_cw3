#include "globals.h"

#include <QLabel>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>
namespace{

class Tab1 : public QWidget{

public:
	Tab1() : QWidget(){
		QHBoxLayout* h=new QHBoxLayout;
		h->addWidget(new QLabel("left"));
		h->addWidget(new QLabel("right"));
		setLayout(h);
	}

};

INSTALL_TAB(Tab1, __FILE__);

}
