#include "globals.h"
#include "gitpp7.h"
#include <QString>
#include <QLabel>
#include <QHBoxLayout>

namespace{

class HelloWorldLabel : public QWidget{
public:
	HelloWorldLabel() : QWidget(){
		QHBoxLayout* h = new QHBoxLayout;

		h->addWidget(new QLabel("ID"));
		h->addWidget(new QLabel("Author"));
		h->addWidget(new QLabel("Last Commit"));
		h->addWidget(new QLabel("Last Update"));

			//function for commits
		GITPP::REPO r;

		for(GITPP::COMMIT i : r.commits()){
		//h->addWidget(new QLabel(QString::fromStdString(i.c_str())));  //commit hash
		h->addWidget(new QLabel(i.id().c_str()));
		h->addWidget(new QLabel(i.signature().name().c_str()));  //author
		h->addWidget(new QLabel(i.message().c_str())); //commit message
		h->addWidget(new QLabel(i.time().c_str())); //time

		}

		setLayout(h);

	}

};



INSTALL_TAB(HelloWorldLabel, __FILE__);

}
