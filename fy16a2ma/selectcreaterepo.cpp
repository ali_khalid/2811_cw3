#include "selectcreaterepo.h"

#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"

SelectCreateRepo::SelectCreateRepo()
{
    QVBoxLayout* scr = new QVBoxLayout();
    QPushButton* test = new QPushButton(this);
    QPushButton* backToMM = new QPushButton(this);

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    QObject::connect(test, SIGNAL(clicked()),this, SLOT(clickedSlot4()));

    test->setText("Select Or Create A Repository");
    backToMM->setText("Back To Main Menu");

    scr->addWidget(test);
    scr->addWidget(backToMM);

    this->setLayout(scr);
}

void SelectCreateRepo::clickedSlot4(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("SELECT/CREATE REPO");
    msgBox.exec();
}

void SelectCreateRepo::backToMM(){
    tabWindow->setCurrentIndex(0);
}
