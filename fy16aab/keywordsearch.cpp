#include "keywordsearch.h"
#include <QWidget>
#include  <QTabWidget>
#include <QObject>
#include <QtGui>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <iostream>
#include <unistd.h>
#include <QLineEdit>
#include "gitpp7.h"
#include "globals.h"
#include <string>         // std::string
#include <locale>         // std::locale, std::toupper

using namespace ali;
using namespace std;


KeywordSearch::KeywordSearch():QWidget(){

   GITPP::REPO r;
   auto c=r.config();
   GITPP::CONFIG::ITEM N=c["user.name"];
  //  cout << "hello" << N.value() << endl;
  // r.commits().create("Commit1");
  // r.commits().create("Commit2");
  // r.commits().create("Commit3");
  // r.commits().create("Commit4");
cout << "Your Commits" << endl;
for (auto i : r.commits()){
  cout << i << " " << i.signature().name() << " " << i.message()<< endl;
}


    _kws = new QVBoxLayout();
    QHBoxLayout* boxAndSubmitLayout = new QHBoxLayout();
    QVBoxLayout* searchResultsLayout = new QVBoxLayout();

    QPushButton* backToMM = new QPushButton(this);
    QLabel* pageTitle = new QLabel("<h2> Search Keyword in Commits <h2>");
    _editor = new QLineEdit();
    _resultLabel = new QLabel();

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    connect(_editor, SIGNAL(textChanged(const QString &)), this, SLOT(showResults(const QString &)));

    _editor->setPlaceholderText("enter keyword..");
    backToMM->setText("Back To Main Menu");
    _kws->addWidget(pageTitle);
    boxAndSubmitLayout->addWidget(_editor);
    searchResultsLayout->addWidget(_resultLabel);
    _kws->addLayout(boxAndSubmitLayout);
    _kws->addLayout(searchResultsLayout);
    _kws->addStretch(1);
    _kws->addWidget(backToMM);

    this->setLayout(_kws);
}

//void KeywordSearch::clickedSlot2(){
//    QMessageBox msgBox;
//    _keyword = _editor->text().toStdString();
//    _editor->clear();
//    msgBox.setWindowTitle("MessageBox Title");
//    msgBox.setText(QString::fromStdString(_keyword));
//    msgBox.exec();
//}
void KeywordSearch::showResults(const QString &query){
  GITPP::REPO r;
  auto c=r.config();
  GITPP::CONFIG::ITEM N=c["user.name"];

    int success = 0;
    int suc_count=0;
    QString upperQuery;
    std::string test[6];
    string tempUpper;
    string bestMatch;
    // test[0] = "Hello my name is Ahmed";
    // test[1] = "Ali Khalid";
    // test[2] = "Hello my name is Zahra";
    // test[3] = "Hello my name is Areej";
    // test[4] = "Hello my name is AlHusain";
    // test[5] = "TEST";
    upperQuery = query.toUpper();

    for (auto i : r.commits()){
      tempUpper = toUpperCase(i.message());
      std::size_t found = tempUpper.find(upperQuery.toStdString()) ;
      if(found !=std::string::npos){
        bestMatch = i.message();
        suc_count++;
      }
    }

    // for(int i = 0; i<6; i++){
    //     tempUpper = toUpperCase(test[i]);
    //     std::size_t found = tempUpper.find(upperQuery.toStdString()) ;
    //     if(found !=std::string::npos){
    //         success = i;
    //         suc_count++;
    //     }
    // }

    if (query == ""){
        _resultLabel->clear();
    }else{
        if (suc_count > 0){
            if (suc_count > 1){
                std::string suc_str = std::to_string(suc_count);
                _resultLabel->setText(QString::fromStdString("Best-Match Commit Found: \n" + bestMatch) + "\n\n +" +QString::fromStdString(suc_str) +" other commits found.." );
            }else{
                _resultLabel->setText(QString::fromStdString("Best-Match Commit Found: \n" + bestMatch));
            }
        }else{
            _resultLabel->setText(QString::fromStdString("No commits found. Try again! \n"));
        }
    }
}


void KeywordSearch::backToMM(){
    tabWindow->setCurrentIndex(0);
}

string KeywordSearch::toUpperCase(string s){
    string upper;
    for (string::size_type i = 0; i < s.length(); i++){
        upper += toupper(s[i]);
    }
    return upper;
}
