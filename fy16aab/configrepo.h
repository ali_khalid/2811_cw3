#ifndef CONFIGREPO_H
#define CONFIGREPO_H

#include <QWidget>
#include <QTabWidget>
class ConfigRepo : public QWidget
{
    Q_OBJECT
public:
    ConfigRepo();
private slots:
    void clickedSlot1();
    void backToMM();

};

#endif // CONFIGREPO_H
