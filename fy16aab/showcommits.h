#ifndef SHOWCOMMITS_H
#define SHOWCOMMITS_H

#include <QWidget>
#include <QTabWidget>
class ShowCommits : public QWidget
{
    Q_OBJECT
public:
    ShowCommits();
private slots:
    void clickedSlot5();
    void backToMM();

};

#endif // SHOWCOMMITS_H
