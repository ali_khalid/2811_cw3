#ifndef BRANCHNAV_H
#define BRANCHNAV_H
#include <QWidget>
#include <QTabWidget>
class BranchNav : public QWidget
{
    Q_OBJECT
public:
    BranchNav();
private slots:
    void clickedSlot();
    void backToMM();
};

#endif // BRANCHNAV_H
