#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
class window : public QMainWindow
{
    Q_OBJECT
public:
    window();
private slots:
    void pushbutton_click();
    void clickedSlot2();
};

#endif // WINDOW_H
