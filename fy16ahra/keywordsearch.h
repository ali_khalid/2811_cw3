//#ifndef KEYWORDSEARCH_H
//#define KEYWORDSEARCH_H
#pragma once
//#include <QObject>
#include <QWidget>
#include <QTabWidget>
#include <QLineEdit>
#include <QBoxLayout>
#include <QLabel>

namespace ali{

class KeywordSearch :public QWidget
{
    Q_OBJECT
public:
    KeywordSearch();
    std::string toUpperCase(std::string s);

private slots:
    void backToMM();
    void showResults(const QString &query);
private:
    QLineEdit* _editor;
    std::string _keyword;
    QVBoxLayout* _kws;
    QLabel* _resultLabel;
  };
}
//#endif // KEYWORDSEARCH_H
