#include "mainmenu.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"

MainMenu::MainMenu(){

    QVBoxLayout* mm = new QVBoxLayout();

    QPushButton* test = new QPushButton(this);
    QPushButton* goToShowCommits = new QPushButton(this);
    QPushButton* goToSearchKeyword = new QPushButton(this);
    QPushButton* goToBranchNav = new QPushButton(this);
    QPushButton* goToSelCreateRepo = new QPushButton(this);
    QPushButton* goToConfigRepo = new QPushButton(this);

    QObject::connect(test, SIGNAL(clicked()),this, SLOT(clickedSlot3()));
    QObject::connect(goToShowCommits, SIGNAL(clicked()),this, SLOT(goToShowCommits()));
    QObject::connect(goToSearchKeyword, SIGNAL(clicked()),this, SLOT(goToSearchKeyword()));
    QObject::connect(goToBranchNav, SIGNAL(clicked()),this, SLOT(goToBranchNav()));
    QObject::connect(goToSelCreateRepo, SIGNAL(clicked()),this, SLOT(goToSelCreateRepo()));
    QObject::connect(goToConfigRepo, SIGNAL(clicked()),this, SLOT(goToConfigRepo()));

    test->setText("Main Menu");
    goToShowCommits->setText("Display Commits");
    goToSearchKeyword->setText("Search Keyword");
    goToBranchNav->setText("Navigate Branches");
    goToSelCreateRepo->setText("Select/Create Repository");
    goToConfigRepo->setText("Configure Repository");


    mm->addWidget(test);
    mm->addWidget(goToSelCreateRepo);
    mm->addWidget(goToBranchNav);
    mm->addWidget(goToConfigRepo);
    mm->addWidget(goToSearchKeyword);
    mm->addWidget(goToShowCommits);

    this->setLayout(mm);
}

void MainMenu::clickedSlot3(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("MAIN MENU");
    msgBox.exec();
}

void MainMenu::goToShowCommits(){
    tabWindow->setCurrentIndex(5);
}
void MainMenu::goToSearchKeyword(){
    tabWindow->setCurrentIndex(4);
}
void MainMenu::goToBranchNav(){
    tabWindow->setCurrentIndex(2);
}
void MainMenu::goToSelCreateRepo(){
    tabWindow->setCurrentIndex(1);
}
void MainMenu::goToConfigRepo(){
    tabWindow->setCurrentIndex(3);
}
