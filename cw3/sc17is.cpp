#include "sc17is.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
//#include "gitpp7.h"
#include "globals.h"

ShowCommits::ShowCommits()
{
//    GITPP::REPO r;
    QVBoxLayout* sc = new QVBoxLayout();
    QPushButton* test = new QPushButton(this);
    QPushButton* backToMM = new QPushButton(this);

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    QObject::connect(test, SIGNAL(clicked()),this, SLOT(clickedSlot5()));

    test->setText("Display All Commits");
    backToMM->setText("Back To Main Menu");

    sc->addWidget(test);
    sc->addWidget(backToMM);

    this->setLayout(sc);
}

void ShowCommits::clickedSlot5(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("DISPLAY COMMITS");
    msgBox.exec();
}

void ShowCommits::backToMM(){
    tabWindow->setCurrentIndex(0);
}
