
#include <QApplication>
#include <QWidget>
#include <QBoxLayout>
#include <QPushButton>
#include <QTabWidget>
#include <iostream>
#include <unistd.h>
#include "window.h"
//int argc=0;
//char* argv[] = {NULL};

int main(int argc, char *argv[])
{

	QApplication app(argc, argv);
    window HelloWindow;
    app.exec();

	// avoid segfault in QtWidget destructors.
	// _exit(0);
}
