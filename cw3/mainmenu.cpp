#include "mainmenu.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"
#include <QLabel>

MainMenu::MainMenu(){

    QVBoxLayout* mm = new QVBoxLayout();

    QLabel* pageTitle = new QLabel(this);
    QPushButton* goToShowCommits = new QPushButton(this);
    QPushButton* goToSearchKeyword = new QPushButton(this);
    QPushButton* goToBranchNav = new QPushButton(this);
    QPushButton* goToRepoStatistics = new QPushButton(this);
    QPushButton* goToConfigRepo = new QPushButton(this);

    QObject::connect(goToShowCommits, SIGNAL(clicked()),this, SLOT(goToShowCommits()));
    QObject::connect(goToSearchKeyword, SIGNAL(clicked()),this, SLOT(goToSearchKeyword()));
    QObject::connect(goToBranchNav, SIGNAL(clicked()),this, SLOT(goToBranchNav()));
    QObject::connect(goToRepoStatistics, SIGNAL(clicked()),this, SLOT(goToRepoStatistics()));
    QObject::connect(goToConfigRepo, SIGNAL(clicked()),this, SLOT(goToConfigRepo()));

    pageTitle->setText("Main Menu");
    pageTitle->setFont(QFont("TypeWriter", 18));
    pageTitle->setAlignment(Qt::AlignCenter);
    pageTitle->show();
    goToShowCommits->setText("Display Commits");
    goToSearchKeyword->setText("Search Keyword");
    goToBranchNav->setText("Navigate Branches");
    goToRepoStatistics->setText("Repository Statistics");
    goToConfigRepo->setText("Configure Repository");


    mm->addWidget(pageTitle);
    mm->addWidget(goToRepoStatistics);
    mm->addWidget(goToBranchNav);
    mm->addWidget(goToConfigRepo);
    mm->addWidget(goToSearchKeyword);
    mm->addWidget(goToShowCommits);

    this->setLayout(mm);
}

void MainMenu::goToShowCommits(){
    tabWindow->setCurrentIndex(5);
}
void MainMenu::goToSearchKeyword(){
    tabWindow->setCurrentIndex(4);
}
void MainMenu::goToBranchNav(){
    tabWindow->setCurrentIndex(2);
}
void MainMenu::goToRepoStatistics(){
    tabWindow->setCurrentIndex(1);
}
void MainMenu::goToConfigRepo(){
    tabWindow->setCurrentIndex(3);
}
