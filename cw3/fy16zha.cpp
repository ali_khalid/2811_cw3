#include "fy16zha.h"
#include "gitpp7.h"
#include "time.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"
#include <QLineEdit>
#include <QLabel>

RepoStatistics::RepoStatistics()
{
  QObject::connect(tabWindow,SIGNAL(currentChanged(int)), this, SLOT(tabUpdate()));
  QVBoxLayout* RS = new QVBoxLayout();
  QPushButton* backToMM = new QPushButton(this);
  QLabel* label = new QLabel(this);
  QLabel* label1 = new QLabel(this);
  QLabel* label2 = new QLabel(this);
  QLabel* label3 = new QLabel(this);
  QLabel* label4 = new QLabel(this);
  QLabel* label5 = new QLabel(this);
  QLabel* label6 = new QLabel(this);

  name = new QLabel(this);
  email = new QLabel(this);
  commits = new QLabel(this);
  branches = new QLabel(this);
  timeC_first = new QLabel(this);
  timeC_last = new QLabel(this);


  QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
  backToMM->setText("Back To Main Menu");


  label->setText("Repository Statistics");
  label->setFont(QFont("TypeWriter", 18));
  label->show();

  label1->setText("Name: ");
  label1->setFont(QFont("TypeWriter", 14));
  label1->show();
  name->show();


  label2->setText("E-mail: ");
  label2->setFont(QFont("TypeWriter", 14));
  label2->show();
  email->show();


  label3->setText("Number of branches: ");
  label3->setFont(QFont("TypeWriter", 14));
  label3->show();
  branches->show();
  // r.branches().create("branch2");
  // r.checkout("branch2");
  // r.commits().create("TEST2");


  // std::cout << "\n\n ***************** Number of Branches::::: " << numB << std::endl;



  label4->setText("Number of commits: ");
  label4->setFont(QFont("TypeWriter", 14));
  label4->show();
  commits->show();
  //r.commits().create("Test");



  label5->setText("First commit at: ");
  label5->setFont(QFont("TypeWriter", 14));
  label5->show();
  timeC_first->show();

  label6->setText("Last commit at: ");
  label6->setFont(QFont("TypeWriter", 14));
  label6->show();
  timeC_last->show();

  RS->addWidget(label);
  RS->addWidget(label1);
  RS->addWidget(name);
  RS->addWidget(label2);
  RS->addWidget(email);
  RS->addWidget(label3);
  RS->addWidget(branches);
  RS->addWidget(label4);
  RS->addWidget(commits);
  RS->addWidget(label5);
  RS->addWidget(timeC_first);
  RS->addWidget(label6);
  RS->addWidget(timeC_last);
  RS->addWidget(backToMM);
  this->setLayout(RS);
}

void RepoStatistics::backToMM(){
    tabWindow->setCurrentIndex(0);
}

void RepoStatistics::tabUpdate(){

  GITPP::REPO r;
  auto c=r.config();

  std::string last_b_name;
  std::string last_c_message;

  GITPP::CONFIG::ITEM N=c["user.name"];
  GITPP::CONFIG::ITEM E=c["user.email"];
  name->setText(QString::fromStdString(N.value()));
  name->show();
  email->setText(QString::fromStdString(E.value()));
  email->show();

  int numB = 0;

  for(GITPP::BRANCH i : r.branches()){
    last_b_name = i.name();
    numB++;
  }
  int numC = 0;

  for(GITPP::COMMIT i : r.commits()){
    last_c_message = i.message();
    numC++;
  }

  branches->setText(QString::number(numB));
  branches->show();
  commits->setText(QString::number(numC));
  commits->show();

  int count = 0;
  std::string t_first;
  std::string t_last;

  for(GITPP::COMMIT i : r.commits()){
    if(count ==0){
      t_last = i.time().c_str();
    }
   t_first = i.time().c_str();
   count++;
  }
  timeC_first->setText(QString::fromStdString(t_first));
  timeC_last->setText(QString::fromStdString(t_last));


}
