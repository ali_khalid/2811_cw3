#ifndef REPOSTATISTICS_H
#define REPOSTATISTICS_H

#include <QWidget>
#include <QTabWidget>
#include <QLabel>
class RepoStatistics : public QWidget
{
    Q_OBJECT
public:
    RepoStatistics();
private slots:
    void backToMM();
    void tabUpdate();
private:
  QLabel* name;
  QLabel* email;
  QLabel* commits;
  QLabel* branches;
  QLabel* timeC_first;
  QLabel* timeC_last;

};

#endif // REPOSTATISTICS_H
