#ifndef CONFIGREPO_H
#define CONFIGREPO_H

#include "gitpp7.h"
#include <QWidget>
#include <QTabWidget>
#include <QLineEdit>
#include <QLabel>



//namespace areej{

  class ConfigRepo : public QWidget
  {
      Q_OBJECT
  public:
      ConfigRepo();
  private slots:
      void clickedSlot1();
      void clickedSlot2();
      void backToMM();
      //QString clickedSlot1();
      //void clickedSlot3(QString* str);

  private:
      QLineEdit* ntext;
      QLineEdit* etext;
      QLabel* _name;
      QLabel* _email;

  };
//}

#endif // CONFIGREPO_H
