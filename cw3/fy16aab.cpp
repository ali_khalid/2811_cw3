#include "fy16aab.h"
#include <QWidget>
#include  <QTabWidget>
#include <QObject>
#include <QtGui>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <iostream>
#include <unistd.h>
#include <QLineEdit>
#include "gitpp7.h"
#include "globals.h"
#include <string>         // std::string
#include <locale>         // std::locale, std::toupper

using namespace ali;
using namespace std;


KeywordSearch::KeywordSearch():QWidget(){
  GITPP::REPO r;
  auto c = r.config();

    _kws = new QVBoxLayout();
    QHBoxLayout* boxAndSubmitLayout = new QHBoxLayout();
    QVBoxLayout* searchResultsLayout = new QVBoxLayout();

    QPushButton* backToMM = new QPushButton(this);
    QLabel* pageTitle = new QLabel(" Search Keyword in Commits ");
    pageTitle->setFont(QFont("TypeWriter", 18));
    _editor = new QLineEdit();
    _resultLabel = new QLabel();
    _nameLabel = new QLabel();
    _msgLabel = new QLabel();
    _timeLabel = new QLabel();

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    connect(_editor, SIGNAL(textChanged(const QString &)), this, SLOT(showResults(const QString &)));

    _editor->setPlaceholderText("enter keyword..");
    backToMM->setText("Back To Main Menu");
    _kws->addWidget(pageTitle);
    boxAndSubmitLayout->addWidget(_editor);
    searchResultsLayout->addWidget(_nameLabel);
    searchResultsLayout->addWidget(_timeLabel);
    searchResultsLayout->addWidget(_msgLabel);
    searchResultsLayout->addStretch(1);
    searchResultsLayout->addWidget(_resultLabel);
    _kws->addLayout(boxAndSubmitLayout);
    _kws->addLayout(searchResultsLayout);
    _kws->addStretch(1);
    _kws->addWidget(backToMM);

    this->setLayout(_kws);
}

void KeywordSearch::showResults(const QString &query){

  GITPP::REPO r;
  auto c=r.config();


    int suc_count=0;
    QString upperQuery;
    string tempUpper;
    string nameMatch;
    string timeMatch;
    string msgMatch;


    upperQuery = query.toUpper();
    for (auto i : r.commits()){
      tempUpper = toUpperCase(i.message());
      std::size_t found = tempUpper.find(upperQuery.toStdString()) ;
      if(found !=std::string::npos){
        nameMatch= i.signature().name();
        timeMatch= i.time().c_str();
        msgMatch=i.message();
        suc_count++;
      }
    }

    if (query == ""){
        _resultLabel->clear();
        _nameLabel->clear();
        _msgLabel->clear();
        _timeLabel->clear();
    }else{
        if (suc_count > 0){
            if (suc_count > 1){
              _nameLabel->clear();
              _msgLabel->clear();
              _timeLabel->clear();
              _resultLabel->clear();
                std::string suc_str = std::to_string(suc_count-1);
                _nameLabel->setText(QString::fromStdString("Author:    " + nameMatch));
                _timeLabel->setText(QString::fromStdString("Time of commit:    " + timeMatch));
                _msgLabel->setText(QString::fromStdString("Commit message:    " + msgMatch));
                _resultLabel->setText(QString::fromStdString("\n\n\n+"+suc_str + " Other matching commits found.."));


            }else{
              _nameLabel->clear();
              _msgLabel->clear();
              _timeLabel->clear();
              _resultLabel->clear();
              _nameLabel->setText(QString::fromStdString("Author:    " + nameMatch));
              _timeLabel->setText(QString::fromStdString("Time of commit:    " + timeMatch));
              _msgLabel->setText(QString::fromStdString("Commit message:    " + msgMatch));            }
        }else{
          _nameLabel->clear();
          _msgLabel->clear();
          _timeLabel->clear();
          _resultLabel->clear();
            _resultLabel->setText(QString::fromStdString("No commits found. Try again! \n"));
        }
    }
}


void KeywordSearch::backToMM(){
    tabWindow->setCurrentIndex(0);
}

string KeywordSearch::toUpperCase(string s){
    string upper;
    for (string::size_type i = 0; i < s.length(); i++){
        upper += toupper(s[i]);
    }
    return upper;
}
