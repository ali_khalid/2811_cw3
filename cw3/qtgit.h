
#pragma once
#include "gitpp7.h"
#include <QObject>
#include <QWidget>

class QtGit : public QtObject{
Q_OBJECT
public:
QtGit();
public: // stuff
void OpenRepo(const std::string&);
GITPP::REPO& repo();

signals: // notify others
void repoOpened();
void repoClosed();
void branchChanged();

private:
GITPP::REPO* _repo;
};
