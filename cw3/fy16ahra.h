#ifndef BRANCHNAV_H
#define BRANCHNAV_H
#include <QWidget>
#include <QTabWidget>
#include <QListWidget>

class BranchNav : public QWidget
{
    Q_OBJECT
public:
    BranchNav();
private slots:
    void clickedSlot();
    void clickedSlot2(QListWidgetItem* item);

    void backToMM();

  private:
      QLineEdit* ctext;
      QListWidget* list ;

};

#endif // BRANCHNAV_H
