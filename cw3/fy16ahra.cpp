#include "fy16ahra.h"
#include "gitpp7.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include  <QLabel>
#include  <QString>
#include <string>
#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>
#include "string.h"
#include <QMessageBox>
#include <QDialog>

#include "globals.h"

BranchNav::BranchNav()
{

  GITPP::REPO r;

    QVBoxLayout* bn = new QVBoxLayout();
    QVBoxLayout* br = new QVBoxLayout();
    QHBoxLayout* bc = new QHBoxLayout();
    QPushButton* backToMM = new QPushButton(this);
    QPushButton* create = new QPushButton(this);
    QLabel* label =new QLabel();
    list = new QListWidget(this);
    QLabel* text =new QLabel();
    QLabel* textc =new QLabel();
    ctext = new QLineEdit(this);
    //r.commits().create("Commit0");
    // r.commits().create("");

//r.branches().create("eee");
//r.commits().create("Commit0");
//r.branches().create("branch1");
//r.checkout("eee");
//r.commits().create("commit1");
//r.commits().create("hessa");
// r.checkout("aaaa");
// r.commits().create("AAAAAAAAAAAAAAAAAAAAAA");
// r.commits().create("my commit 55");


    for(GITPP::BRANCH i : r.branches()){
            new QListWidgetItem(QString::fromStdString(i.name()), list);
       }


    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    QObject::connect(list, SIGNAL(itemClicked(QListWidgetItem*)),this, SLOT(clickedSlot2(QListWidgetItem*)));
    QObject::connect(create, SIGNAL(clicked()),this, SLOT(clickedSlot()));


    label->setText("Branch Navigation");
    label->setFont(QFont("TypeWriter", 18));
    text->setText("Select a branch : ");
    textc->setText("Create a branch : ");

    backToMM->setText("Back To Main Menu");
    create->setText("create");

    ctext->setPlaceholderText("new branch name...");

    br->addWidget(text);
    br->addWidget(list);
    bc->addWidget(textc);
    bc->addWidget(ctext);
    bn->addWidget(label);
    bn->addLayout(br);
    bn->addLayout(bc);
    bn->addWidget(create);
    bn->addWidget(backToMM);

    this->setLayout(bn);
}

void BranchNav::clickedSlot(){

    GITPP::REPO r;
    QMessageBox msgBox;
        msgBox.setText("Creat New branch " + ctext->text() + " ?"   );
        msgBox.setInformativeText("Do you want to save your changes?");
        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
        msgBox.setDefaultButton(QMessageBox::Save);
        int confirm = msgBox.exec();

        switch (confirm) {
          case QMessageBox::Save:
            r.branches().create(ctext->text().toStdString());
            list->addItem(ctext->text());
            list->repaint();

              break;
          case QMessageBox::Cancel:
              // no changes should be made
              break;
          default:
              // no changes
              break;
            }

}

void BranchNav::clickedSlot2(QListWidgetItem* item){
    GITPP::REPO r;
    auto c=r.config();
    r.checkout(item->text().toStdString());

    QDialog* dialog =new QDialog();
    QVBoxLayout* lay = new QVBoxLayout();

    dialog->setWindowTitle("Branch Navigation");
    QLabel* tex =new QLabel();
    lay->addWidget(tex);

    tex->setText(item->text()+ " commits :");
    for(auto i : r.commits()){
      lay->addWidget(new QLabel(QString::fromStdString(i.signature().name()) + " " + QString::fromStdString(i.message())));
     }

    dialog->setLayout(lay);
    dialog->exec();

}


void BranchNav::backToMM(){
    tabWindow->setCurrentIndex(0);
}
