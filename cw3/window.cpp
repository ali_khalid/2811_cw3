#include "window.h"
#include "fy16zha.h"
#include "fy16aab.h"
#include "fy16ahra.h"
#include "fy16a2ma.h"
#include "sc17is.h"
#include "mainmenu.h"
#include <QWidget>
#include  <QTabWidget>
#include <QBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include "globals.h"
#include <QToolBar>
#include <QString>
#include <QDesktopWidget>
#include "gitpp7.h"
#include <string>

using namespace ali;
QTabWidget* tabWindow;
window::window()
{
try{
  std::string path=".";
  GITPP::REPO r(path.c_str());
  auto c=r.config();
  std::cout << "======= Repository available! =======\n\n";
} catch(GITPP::EXCEPTION_CANT_FIND const&){

  int status = 0;

  QMessageBox msgBox;
  msgBox.setText("No repo present in this directory..");
  msgBox.setInformativeText("Do you want to create a repo here?");
  msgBox.setStandardButtons(QMessageBox::Close | QMessageBox::Yes);
  msgBox.setDefaultButton(QMessageBox::Yes);
  int confirm = msgBox.exec();

  switch (confirm) {
    case QMessageBox::Yes:
        // change the email
        status = 1;
        break;
    case QMessageBox::Close:
       // no changes should be made
       exit(1);
       status = 2;
        break;
    default:
       // no changes
        break;
  }
  if(status == 1){ //create repo
    std::string path=".";
    GITPP::REPO r(GITPP::REPO::_create, path.c_str()); //Create new repo
  }
}
    this->setFixedSize(500,500);
    tabWindow = new QTabWidget();
    MainMenu* mainMenuTab = new MainMenu();
    KeywordSearch* keywordSearchTab= new KeywordSearch();
    ConfigRepo* repoTab = new ConfigRepo();
    ShowCommits* showCommitsTab = new ShowCommits();
    BranchNav* branchNavTab = new BranchNav();
    RepoStatistics* repoStatisticsTab = new RepoStatistics();

    tabWindow->addTab(mainMenuTab, "Main Menu");
    tabWindow->addTab(repoStatisticsTab, "FY16ZHA");
    tabWindow->addTab(branchNavTab, "FY16AHRA");
    tabWindow->addTab(repoTab, "FY16A2MA");
    tabWindow->addTab(keywordSearchTab, "FY16AAB");
    tabWindow->addTab(showCommitsTab, "SC17IS");

    tabWindow->show();
}

void window::pushbutton_click(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("TESTING");
    msgBox.exec();
}

void window::clickedSlot2(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("TESTING");
    msgBox.exec();
}
