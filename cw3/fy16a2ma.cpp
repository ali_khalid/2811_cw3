#include "fy16a2ma.h"
#include "gitpp7.h"
#include <QWidget>
#include <QTabWidget>
#include <QBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include "globals.h"
#include <QGroupBox>
#include <QTextEdit>
#include <string>

//using namespace areej;
//using namespace std;

ConfigRepo::ConfigRepo()
{

  GITPP::REPO r;
  auto c=r.config();
  GITPP::CONFIG::ITEM N=c["user.name"];
  GITPP::CONFIG::ITEM E=c["user.email"];

    QHBoxLayout* cn1 = new QHBoxLayout();
    QHBoxLayout* ce1 = new QHBoxLayout();
    QHBoxLayout* cn = new QHBoxLayout();
    QHBoxLayout* ce = new QHBoxLayout();
    QVBoxLayout* cr = new QVBoxLayout();

    _name = new QLabel(this);
    _email = new QLabel(this);
    ntext = new QLineEdit(this);
    etext = new QLineEdit(this);
    QLabel* clabel = new QLabel(this);
    QPushButton* change_n = new QPushButton(this);
    QPushButton* change_e = new QPushButton(this);
    QPushButton* backToMM = new QPushButton(this);

    QObject::connect(change_n, SIGNAL(clicked()),this, SLOT(clickedSlot1()));
    QObject::connect(change_e, SIGNAL(clicked()),this, SLOT(clickedSlot2()));
    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));

    clabel->setText(" Repository Configuration ");
    clabel->setFont(QFont("TypeWriter", 18));
    _name->setText(QString::fromStdString("Name : " + N.value()));
    _email->setText(QString::fromStdString("Email : " + E.value()));

    ntext->setPlaceholderText("new name..");
    etext->setPlaceholderText("new email..");

    change_n->setText("Change Name");
    change_e->setText("Change Email");
    backToMM->setText("Back To Main Menu");


    cn1->addWidget(_name);
    ce1->addWidget(_email);

    cn->addWidget(ntext);
    cn->addWidget(change_n);

    ce->addWidget(etext);
    ce->addWidget(change_e);

    cr->addWidget(clabel);
    cr->addLayout(cn1);
    cr->addLayout(ce1);
    cr->addLayout(cn);
    cr->addLayout(ce);
    cr->addWidget(backToMM);

    this->setLayout(cr);

}


void ConfigRepo::clickedSlot1(){

  if ( ntext->text() == "" ){
    QMessageBox::warning(this, tr(" "),
    tr("Please enter your name! "), QMessageBox::Ok);
    return;

  }else {

    GITPP::REPO r;
    auto c=r.config();
    GITPP::CONFIG::ITEM N=c["user.name"];

    QMessageBox msgBox;
    msgBox.setText("The name has been modified to : " + ntext->text());
    msgBox.setInformativeText("Do you want to save your changes?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);
    int confirm = msgBox.exec();

        switch (confirm) {
          case QMessageBox::Save:
              // change name
            _name->setText("Name : " + ntext->text());
            N = ntext->text().toStdString();
              break;
          case QMessageBox::Cancel:
              // no changes should be made
              break;
          default:
              // no changes
              break;
        }
  }
}


void ConfigRepo::clickedSlot2(){

  QRegularExpression r_exp("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b",QRegularExpression::CaseInsensitiveOption);
  etext->setValidator(new QRegularExpressionValidator(r_exp, this));

  if ( etext->text() == "" ){
    QMessageBox::warning(this, tr(" "),
    tr("Please enter your Email! "), QMessageBox::Ok);
    return;

  }else if(!etext->hasAcceptableInput()){
    QMessageBox::warning(this, tr(" "),
    tr("Email format is incorrect.. Please Enter a valid Email "), QMessageBox::Ok);
    return;

  }else {

      GITPP::REPO r;
      auto c=r.config();
      GITPP::CONFIG::ITEM E=c["user.email"];

      QMessageBox msgBox;
      msgBox.setText("The email has been modified to : " + etext->text());
      msgBox.setInformativeText("Do you want to save your changes?");
      msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
      msgBox.setDefaultButton(QMessageBox::Save);
      int confirm = msgBox.exec();

      switch (confirm) {
        case QMessageBox::Save:
            // change the email
          _email->setText("Email : " + etext->text());
          E = etext->text().toStdString();
            break;
        case QMessageBox::Cancel:
           // no changes should be made
            break;
        default:
           // no changes
            break;
      }
    }
}

void ConfigRepo::backToMM(){
    tabWindow->setCurrentIndex(0);
}
