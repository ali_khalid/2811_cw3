#include "branchnav.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"

BranchNav::BranchNav()
{
    QVBoxLayout* bn = new QVBoxLayout();
    QPushButton* test = new QPushButton(this);
    QPushButton* backToMM = new QPushButton(this);

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    QObject::connect(test, SIGNAL(clicked()),this, SLOT(clickedSlot()));

    test->setText("Navigate Branches");
    backToMM->setText("Back To Main Menu");

    bn->addWidget(test);
    bn->addWidget(backToMM);

    this->setLayout(bn);
}

void BranchNav::clickedSlot(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("NAVIGATE BRANCHES");
    msgBox.exec();
}

void BranchNav::backToMM(){
    tabWindow->setCurrentIndex(0);
}
