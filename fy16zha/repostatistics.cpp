#include "repostatistics.h"
#include "gitpp7.h"
#include "time.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"
#include <QLineEdit>
#include <QLabel>

RepoStatistics::RepoStatistics()
{
    QVBoxLayout* RS = new QVBoxLayout();
    QPushButton* backToMM = new QPushButton(this);
    QLabel* label = new QLabel(this);
    QLabel* label1 = new QLabel(this);
    QLabel* label2 = new QLabel(this);
    QLabel* label3 = new QLabel(this);
    QLabel* label4 = new QLabel(this);
    QLabel* label5 = new QLabel(this);
    QLabel* label6 = new QLabel(this);
    QLabel* name = new QLabel(this);
    QLabel* email = new QLabel(this);
    QLabel* commits = new QLabel(this);
    QLabel* branches = new QLabel(this);
    QLabel* timeC_first = new QLabel(this);
    QLabel* timeC_last = new QLabel(this);

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    backToMM->setText("Back To Main Menu");

    GITPP::REPO r;
    auto c=r.config();
    GITPP::CONFIG::ITEM N=c["user.name"];
    GITPP::CONFIG::ITEM E=c["user.email"];

    label->setText("Repository Statistics");
    label->setFont(QFont("TypeWriter", 18));
    label->show();

    label1->setText("Name: ");
    label1->setFont(QFont("TypeWriter", 14));
    label1->show();
    name->setText(QString::fromStdString(N.value()));
    name->show();

    label2->setText("E-mail: ");
    label2->setFont(QFont("TypeWriter", 14));
    label2->show();
    email->setText(QString::fromStdString(E.value()));
    email->show();

    label3->setText("Number of branches: ");
    label3->setFont(QFont("TypeWriter", 14));
    label3->show();
    int numB = 0;
    // r.branches().create("Test");
    r.commits().create("TESTING1");
    for(GITPP::BRANCH i : r.branches()){
      numB++;
    }
    branches->setText(QString::number(numB));
    branches->show();

    label4->setText("Number of commits: ");
    label4->setFont(QFont("TypeWriter", 14));
    label4->show();
    int numC = 0;
    //r.commits().create("Test");
    for(GITPP::COMMIT i : r.commits()){
      numC++;
    }
    commits->setText(QString::number(numC));
    commits->show();

    label5->setText("First commit at: ");
    label5->setFont(QFont("TypeWriter", 14));
    label5->show();
    int count = 0;
    std::string t_first;
    std::string t_last;

    for(GITPP::COMMIT i : r.commits()){
      if(count ==0){
        t_last = i.time().c_str();
      }
     t_first = i.time().c_str();
     count++;
    }
    timeC_first->setText(QString::fromStdString(t_first));
    timeC_first->show();

    label6->setText("Last commit at: ");
    label6->setFont(QFont("TypeWriter", 14));
    label6->show();
    timeC_last->setText(QString::fromStdString(t_last));
    timeC_last->show();

    RS->addWidget(label);
    RS->addWidget(label1);
    RS->addWidget(name);
    RS->addWidget(label2);
    RS->addWidget(email);
    RS->addWidget(label3);
    RS->addWidget(branches);
    RS->addWidget(label4);
    RS->addWidget(commits);
    RS->addWidget(label5);
    RS->addWidget(timeC_first);
    RS->addWidget(label6);
    RS->addWidget(timeC_last);
    RS->addWidget(backToMM);
    this->setLayout(RS);
}

// void RepoStatistics::clickedSlot(){
//     QMessageBox msgBox;
//     std::string path=".";
//     GITPP::REPO r(GITPP::REPO::_create, path.c_str()); //Create new repo
//     // GITPP::REPO r(path.c_str());
//     auto c=r.config();
//     msgBox.setWindowTitle("Create A new Repository");
//     msgBox.setText("Repository created successfully!");
//     msgBox.exec();
// }

void RepoStatistics::backToMM(){
    tabWindow->setCurrentIndex(0);
}
