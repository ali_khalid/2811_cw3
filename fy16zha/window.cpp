#include "window.h"
#include "configrepo.h"
#include "showcommits.h"
#include "keywordsearch.h"
#include "branchnav.h"
#include "repostatistics.h"
#include "mainmenu.h"
#include <QWidget>
#include  <QTabWidget>
#include <QBoxLayout>
#include <QMessageBox>
#include <QPushButton>
#include "globals.h"
#include <QToolBar>
#include <QString>
#include <QDesktopWidget>

using namespace ali;
QTabWidget* tabWindow;
window::window()
{
//    QDesktopWidget* dw = new QDesktopWidget();
//    double sizeFactor = 100;
//    int x = int(dw->width()*sizeFactor);
//    int y = int(dw->height()*sizeFactor);
    this->setFixedSize(500,500);
    tabWindow = new QTabWidget();
    MainMenu* mainMenuTab = new MainMenu();
    KeywordSearch* keywordSearchTab= new KeywordSearch();
    ConfigRepo* repoTab = new ConfigRepo();
    ShowCommits* showCommitsTab = new ShowCommits();
    BranchNav* branchNavTab = new BranchNav();
    RepoStatistics* repoStatisticsTab = new RepoStatistics();

    tabWindow->addTab(mainMenuTab, "Main Menu");
    tabWindow->addTab(repoStatisticsTab, "Repository Statistics");
    tabWindow->addTab(branchNavTab, "Navigate Branches");
    tabWindow->addTab(repoTab, "Config Repository");
    tabWindow->addTab(keywordSearchTab, "Search Keyword");
    tabWindow->addTab(showCommitsTab, "Display Commits");

    tabWindow->show();
}

void window::pushbutton_click(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("TESTING");
    msgBox.exec();
}

void window::clickedSlot2(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("TESTING");
    msgBox.exec();
}
