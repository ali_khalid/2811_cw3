#ifndef MAINMENU_H
#define MAINMENU_H

#include <QWidget>
#include <QTabWidget>
class MainMenu : public QWidget
{
    Q_OBJECT
public:
    MainMenu();
private slots:
    void clickedSlot3();
    void goToShowCommits();
    void goToSearchKeyword();
    void goToBranchNav();
    void goToStatistics();
    void goToConfigRepo();
};

#endif // MAINMENU_H
