#include "configrepo.h"
#include <QWidget>
#include  <QTabWidget>
#include  <QBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include "globals.h"

ConfigRepo::ConfigRepo()
{
    QVBoxLayout* cr = new QVBoxLayout();
    QPushButton* test = new QPushButton(this);
    QPushButton* backToMM = new QPushButton(this);

    QObject::connect(backToMM, SIGNAL(clicked()),this, SLOT(backToMM()));
    QObject::connect(test, SIGNAL(clicked()),this, SLOT(clickedSlot1()));

    test->setText("Repository Configurations");
    backToMM->setText("Back To Main Menu");

    cr->addWidget(test);
    cr->addWidget(backToMM);

    this->setLayout(cr);
}

void ConfigRepo::clickedSlot1(){
    QMessageBox msgBox;
    msgBox.setWindowTitle("MessageBox Title");
    msgBox.setText("SHOW EDIT REPO CONFIG");
    msgBox.exec();
}

void ConfigRepo::backToMM(){
    tabWindow->setCurrentIndex(0);
}
